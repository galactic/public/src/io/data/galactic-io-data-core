=========================
**GALACTIC** io data core
=========================

*galactic-io-data-core* [#logo]_ [#logoiodatacore]_ is the library for dealing
with data and data files.

**GALACTIC** stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

..  toctree::
    :maxdepth: 1
    :caption: Getting started

    install/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Reference

    api/io/data/core/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Release Notes

    release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
        The **GALACTIC** icon has been constructed using icons present in the
        `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
        designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
        The product or characters depicted in these icons
        are © by Disney / Lucasfilm.

        It's a tribute to the french mathematician
        `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
        died at age 20 in a duel.
        In France, Concept Lattices are also called *Galois Lattices*.

.. [#logoiodatacore]
        The *galactic-io-data-core* icon has been constructed using the main
        **GALACTIC** icon,
        the
        `Floppy, save, guardar icon <https://www.iconfinder.com/icons/326688/floppy_save_guardar_icon>`_
        designed by
        `Google <https://www.iconfinder.com/iconsets/material-core>`_,
        the
        `Database, data, storage icon <https://www.iconfinder.com/icons/8541628/database_data_storage_icon>`_
        designed by
        ` Font Awesome Search Icons <https://www.iconfinder.com/iconsets/font-awesome-solid-vol-1>`_
        and the
        `Heart icon <https://www.iconfinder.com/icons/111093/heart_icon>`_ designed by
        `WPZOOM <https://www.iconfinder.com/iconsets/wpzoom-developer-icon-set>`_.

