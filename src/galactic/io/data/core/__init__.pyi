from abc import abstractmethod
from collections.abc import Iterator, Mapping
from typing import Protocol, TextIO, runtime_checkable

from galactic.helpers.core import Population

@runtime_checkable
class DataReader(Protocol):
    @classmethod
    @abstractmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]: ...
    @classmethod
    @abstractmethod
    def extensions(cls) -> Iterator[str]: ...

@runtime_checkable
class DataWriter(Protocol):
    @classmethod
    @abstractmethod
    def write(cls, data_file: TextIO, data: Mapping[str, object]) -> None: ...
    @classmethod
    @abstractmethod
    def extensions(cls) -> Iterator[str]: ...

class PopulationFactory:
    @classmethod
    def detect_plugins(cls) -> None: ...
    @classmethod
    def export(cls, stream: TextIO, population: Population) -> None: ...
    @classmethod
    def create(cls, stream: TextIO) -> Population: ...
    @classmethod
    def register_reader(cls, reader: DataReader) -> None: ...
    @classmethod
    def readers(cls) -> Iterator[DataReader]: ...
    @classmethod
    def register_writer(cls, writer: DataWriter) -> None: ...
    @classmethod
    def writers(cls) -> Iterator[DataWriter]: ...
