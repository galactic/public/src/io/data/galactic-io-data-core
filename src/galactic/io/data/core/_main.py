"""Data io main module."""

from __future__ import annotations

from abc import abstractmethod
from pathlib import Path
from typing import TYPE_CHECKING, ClassVar, Protocol, TextIO, runtime_checkable

from galactic.algebras.set import FIFOSet
from galactic.helpers.core import detect_plugins
from galactic.helpers.population import Population

if TYPE_CHECKING:
    from collections.abc import Iterator, Mapping


# pylint: disable=too-few-public-methods
@runtime_checkable
class DataReader(Protocol):
    """
    Data reader.
    """

    @classmethod
    @abstractmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a data file.

        Parameters
        ----------
        data_file
            A readable file.

        Returns
        -------
        Mapping[str, object]
            An mapping over the (identifier, data) couple.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        Note
        ----
        An extension must contain the preceding dot and can be composed of several
        parts.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class DataWriter(Protocol):
    """
    Data writer.
    """

    @classmethod
    @abstractmethod
    def write(cls, data_file: TextIO, data: Mapping[str, object]) -> None:
        """
        Read a data file.

        Parameters
        ----------
        data_file
            A writable file.
        data
            The data.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        Note
        ----
        An extension must contain the preceding dot and can be composed of several
        parts.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


class PopulationFactory:
    """
    It allows to manage Population instances from files.
    """

    _detected: bool = False
    _reader_extensions: ClassVar[dict[str, DataReader]] = {}
    _readers: ClassVar[FIFOSet[DataReader]] = FIFOSet[DataReader]()
    _writer_extensions: ClassVar[dict[str, DataWriter]] = {}
    _writers: ClassVar[FIFOSet[DataWriter]] = FIFOSet[DataWriter]()

    @classmethod
    def detect_plugins(cls) -> None:
        """
        Detect plugins from ``galactic.io.data`` group.
        """
        detect_plugins(group="galactic.io.data")
        cls._detected = True

    @classmethod
    def export(cls, stream: TextIO, population: Population) -> None:
        """
        Write a population to a writable opened file.

        Parameters
        ----------
        stream
            File to write
        population
            The population to export

        Raises
        ------
        ValueError
            If the extension is not recognised

        """
        # pylint: disable=unsubscriptable-object, unsupported-membership-test
        if not cls._detected:
            cls.detect_plugins()
        suffixes = Path(stream.name).suffixes
        for index in range(len(suffixes)):
            extension = "".join(suffixes[index:])
            if extension in cls._writer_extensions:
                cls._writer_extensions[extension].write(stream, population)
                return
        raise ValueError(f"The extension {''.join(suffixes)} is not recognised")

    @classmethod
    def create(cls, stream: TextIO) -> Population:
        """
        Create a population from a readable opened file.

        Parameters
        ----------
        stream
            File to read

        Returns
        -------
        Population
            A new population

        Raises
        ------
        ValueError
            If the extension is not recognised

        """
        # pylint: disable=unsubscriptable-object, unsupported-membership-test
        if not cls._detected:
            cls.detect_plugins()
        suffixes = Path(stream.name).suffixes
        for index in range(len(suffixes)):
            extension = "".join(suffixes[index:])
            if extension in cls._reader_extensions:
                return Population(cls._reader_extensions[extension].read(stream))
        raise ValueError(f"The extension {''.join(suffixes)} is not recognised")

    @classmethod
    def register_reader(cls, reader: DataReader) -> None:
        """
        Register a new reader.

        Parameters
        ----------
        reader
            The reader

        """
        # pylint: disable=unsupported-assignment-operation
        if reader not in cls._readers:
            for extension in reader.extensions():
                cls._reader_extensions[extension] = reader
            cls._readers.add(reader)

    @classmethod
    def readers(cls) -> Iterator[DataReader]:
        """
        Get all the registered data reader plugins.

        Returns
        -------
        Iterator[DataReader]
            An iterator on registered data reader plugins.

        """
        if not cls._detected:
            cls.detect_plugins()
        return iter(cls._readers)

    @classmethod
    def register_writer(cls, writer: DataWriter) -> None:
        """
        Register a new writer.

        Parameters
        ----------
        writer
            The writer

        """
        # pylint: disable=unsupported-assignment-operation
        if writer not in cls._writers:
            for extension in writer.extensions():
                cls._writer_extensions[extension] = writer
            cls._writers.add(writer)

    @classmethod
    def writers(cls) -> Iterator[DataWriter]:
        """
        Get all the registered data reader plugins.

        Returns
        -------
        Iterator[DataWriter]
            An iterator on registered data writer plugins.

        """
        if not cls._detected:
            cls.detect_plugins()
        return iter(cls._writers)
