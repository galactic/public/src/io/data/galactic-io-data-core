"""
:mod:`galactic.io.data.core` module.

It defines two abstract classes:

* :class:`DataReader` for reading data;
* :class:`DataWriter` for writing data.

and one concrete class:

* :class:`PopulationFactory` for dealing with data.
"""

from ._main import DataReader, DataWriter, PopulationFactory

__all__ = (
    "DataReader",
    "DataWriter",
    "PopulationFactory",
)
