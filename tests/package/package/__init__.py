"""Dummy module."""

from ._main import DummyReader, DummyWriter, register

__all__ = (
    "DummyReader",
    "DummyWriter",
)
