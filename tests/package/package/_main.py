"""Package test module."""

from collections.abc import Iterator, Mapping
from typing import IO, AnyStr

from galactic.io.data.core import PopulationFactory


class DummyReader:
    """
    Data reader.
    """

    # noinspection PyUnusedLocal
    @classmethod
    def read(cls, data_file: IO[AnyStr]) -> Mapping[str, object]:
        """
        Read a data file.

        Parameters
        ----------
        data_file
            A readable file.

        Returns
        -------
            An mapping over the (identifier, data) couple.

        """
        return {"key": "value"}

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
            An iterator over the supported extensions

        """
        return iter([".old.tmp", ".tmp"])


class DummyWriter:
    """
    Data writer.
    """

    # noinspection PyUnusedLocal
    @classmethod
    def write(cls, data_file: IO[AnyStr], data: Mapping[str, object]) -> None:
        """
        Read a data file.

        Parameters
        ----------
        data_file
            A writable file.
        data
            The data.

        """
        print("Data saved")

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
            An iterator over the supported extensions

        """
        return iter([".old.tmp", ".tmp"])


def register() -> None:
    """
    Register module.
    """
    PopulationFactory.register_writer(DummyWriter())
    PopulationFactory.register_reader(DummyReader())
