"""Detect plugin test module."""

import subprocess
import sys
from io import StringIO
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch

from galactic.helpers.population import Population
from galactic.io.data.core import PopulationFactory


class PluginsTestCase(TestCase):
    def setUp(self):
        """
        Install dummy package.
        """
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "install",
                Path(__file__).parent / "package",
            ],
        )

    def test_from_file(self):
        pathname = Path(__file__).parent / "test.old.tmp"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(population, {"key": "value"})
        pathname = Path(__file__).parent / "test.tmp"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(population, {"key": "value"})

    @patch("sys.stdout", new_callable=StringIO)
    def test_write(self, stdout: StringIO):
        pathname = Path(__file__).parent / "test.old.tmp"
        population = Population([])
        with pathname.open(mode="w", encoding="utf-8") as data_file:
            PopulationFactory.export(data_file, population)
        self.assertEqual(stdout.getvalue(), "Data saved\n")

    @patch("sys.stdout", new_callable=StringIO)
    def test_write_tmp(self, stdout: StringIO):
        pathname = Path(__file__).parent / "test.tmp"
        population = Population([])
        with pathname.open(mode="w", encoding="utf-8") as data_file:
            PopulationFactory.export(data_file, population)
        self.assertEqual(stdout.getvalue(), "Data saved\n")

    @patch("sys.stdout", new_callable=StringIO)
    def test_write_other(self, stdout: StringIO):
        pathname = Path(__file__).parent / "test.other.tmp"
        population = Population([])
        with pathname.open(mode="w", encoding="utf-8") as data_file:
            PopulationFactory.export(data_file, population)
        self.assertEqual(stdout.getvalue(), "Data saved\n")

    def tearDown(self):
        """
        Uninstall dummy package.
        """
        subprocess.check_call(
            [
                sys.executable,
                "-m",
                "pip",
                "uninstall",
                "-y",
                "package",
            ],
        )
